package Beans;

import java.util.Date;

public class UserDataBeans {

	private String name;
	private String address;
	private String loginId;
	private String password;
	private int id;
	private Date Createdate;


	// コンストラクタ
	public UserDataBeans() {
		this.name = "";
		this.address = "";
		this.loginId = "";
		this.password = "";
	}
	public UserDataBeans(String loginId,String passwords) {
		this.loginId = loginId;
		this.password = passwords;
	}
	public UserDataBeans(String name,String address,String loginId,String password) {
		this.name = name;
		this.address = address;
		this.loginId = loginId;
		this.password = password;
	}

	public UserDataBeans(int id,String name,String address,String loginId,String password,Date Createdate) {
		this.id=id;
		this.name = name;
		this.address = address;
		this.loginId = loginId;
		this.password = password;
		this.Createdate = Createdate;

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * ユーザー情報更新時の必要情報をまとめてセットするための処理
	 *
	 * @param name
	 * @param loginId
	 * @param address
	 */
	public void setUpdateUserDataBeansInfo(String name, String loginId, String address, int id) {
		this.name = name;
		this.loginId = loginId;
		this.address = address;
		this.id = id;
	}


}
