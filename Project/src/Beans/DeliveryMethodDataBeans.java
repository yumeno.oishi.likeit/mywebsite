package Beans;

import java.io.Serializable;

public class DeliveryMethodDataBeans implements Serializable {
	private int id;
	private String name;
	private int price;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	//料金を取り出す
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
}



