package Servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.UserDataBeans;
import Dao.Userdao;



/**
 * Servlet implementation class UpdateResult
 */
@WebServlet("/UpdateResult")
public class UpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* 文字化け対策 */
		request.setCharacterEncoding("UTF-8");
		// セッション開始
		HttpSession session = request.getSession();

		try {

			//ユーザidを取るためにログインIDを持ってくる
			UserDataBeans user=(UserDataBeans) session.getAttribute("user");
			Userdao userdao = new Userdao();
			//ログインIDを元にユーザーIDが含まれたデータを取得
			UserDataBeans user1 =userdao.findloginid(user.getLoginId());

			// 入力フォームから受け取った値をUserDataBeansにセット
			UserDataBeans udb = new UserDataBeans();
			udb.setUpdateUserDataBeansInfo(request.getParameter("user_name_update"), request.getParameter("login_id_update"), request.getParameter("user_address_update"),user1.getId());

			// 確定ボタンが押されたかを確認する変数
			String confirmed = request.getParameter("confirmButton");

			Userdao.updateUser(udb);
			request.setAttribute("udb", udb);
			user.setLoginId(udb.getLoginId());
			session.setAttribute("user", user);
			response.sendRedirect("Mypage");

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

		}


	}


