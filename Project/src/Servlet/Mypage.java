package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.BuyDataBeans;
import Beans.UserDataBeans;
import Dao.BuyDetailDAO;
import Dao.Userdao;


/**
 * Servlet implementation class Mypage
 */
@WebServlet("/Mypage")
public class Mypage extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Mypage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

		//loginIdを受け取る
		//sessionにあるuserデータをget(今ログインしている人のデータ loginId,password)

		UserDataBeans loginuser = (UserDataBeans) session.getAttribute("user");
		//今ログインしている人のloginIDを取得
		String loginId = loginuser.getLoginId();
      //userDaoのインスタンスを作る
        Userdao userDao = new Userdao();
      //findloginidメソッドを呼び出す＋戻り値受け取る
        UserDataBeans user = userDao.findloginid(loginId);

        BuyDetailDAO bdb = new BuyDetailDAO();


    //BuyDataBeans userdetailメソッドを呼び出す＋戻り値受け取る
         ArrayList<BuyDataBeans> buy = bdb.userdetail(user.getId());
         request.setAttribute("buy", buy);


        //受け取った値をjspに持っていくために、受け取った値をリクエストスコープにsetする
        request.setAttribute("mypage", user);

     // フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/myPage.jsp");
		dispatcher.forward(request, response);


		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}


	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
