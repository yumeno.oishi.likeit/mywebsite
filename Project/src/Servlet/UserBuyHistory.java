package Servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.BuyDataBeans;
import Beans.ItemDataBeans;
import Dao.BuyDetailDAO;

/**
 * Servlet implementation class UserBuyHistory
 */
@WebServlet("/UserBuyHistory")
public class UserBuyHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// セッション開始
		HttpSession session = request.getSession();
		try {

			// リクエストパラメータの文字コードを指定
	        request.setCharacterEncoding("UTF-8");

			//request.getParameterはstrig型
	        String id = request.getParameter("buyId");

			int buyId = Integer.parseInt(id);

            //購入日時、配送方法、合計金額BuyDetailDAO
			BuyDetailDAO bdb = new BuyDetailDAO();

			//BuyDataBeans userdetailメソッドを呼び出す＋戻り値受け取る
           BuyDataBeans buyhistory = bdb.userbuyhistory(buyId);
           
           
            request.setAttribute("buyhistory", buyhistory);

            ArrayList<ItemDataBeans> itemList = bdb.itemdetails(buyId);
            request.setAttribute("itemList", itemList);





			// 入力された内容に誤りがあったとき等に表示するエラーメッセージを格納する
			String validationMessage = (String) EcHelper.cutSessionAttribute(session, "validationMessage");


			request.setAttribute("validationMessage", validationMessage);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyDetail.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
