package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Beans.BuyDataBeans;
import Beans.BuyDetailDataBeans;
import Beans.ItemDataBeans;
import model.DBManager;


public class BuyDetailDAO {

	/**
	 * 購入詳細登録処理
	 * @param bddb BuyDetailDataBeans
	 * @throws SQLException
	 * 			呼び出し元にスローさせるため
	 */
	public static void insertBuyDetail(BuyDetailDataBeans bddb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_buy_detail(buy_id,item_id) VALUES(?,?)");
			st.setInt(1, bddb.getBuyId());
			st.setInt(2, bddb.getItemId());
			st.executeUpdate();
			System.out.println("inserting BuyDetail has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
     * 購入IDによる購入詳細情報検索
     * @param buyId
     * @return buyDetailItemList ArrayList<ItemDataBeans>
     *             購入詳細情報のデータを持つJavaBeansのリスト
     * @throws SQLException
     */
	public static ArrayList<ItemDataBeans> getItemDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT m_item.id,"
					+ " m_item.name,"
					+ " m_item.price"
					+ " FROM t_buy_detail"
					+ " JOIN m_item"
					+ " ON t_buy_detail.item_id = m_item.id"
					+ " WHERE t_buy_detail.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> buyDetailItemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));


				buyDetailItemList.add(idb);
			}

			System.out.println("searching ItemDataBeansList by BuyID has been completed");
			return buyDetailItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
			//ユーザー情報購入履歴
			public static BuyDataBeans userbuyhistory(int buyId) throws SQLException{
				Connection con = null;
				PreparedStatement st = null;
				try {
					con = DBManager.getConnection();

					st = con.prepareStatement(
							"SELECT * FROM t_buy JOIN m_delivery_method"
							+ " ON t_buy.delivery_method_id = m_delivery_method.id"
							+ " WHERE t_buy.id = ?");

					st.setInt(1,buyId);

					ResultSet rs = st.executeQuery();

					BuyDataBeans bdb = new BuyDataBeans();


					if (rs.next()) {

						bdb.setId(rs.getInt("id"));
						bdb.setTotalPrice(rs.getInt("total_price")+rs.getInt("price"));
						bdb.setBuyDate(rs.getTimestamp("create_date"));
						bdb.setDelivertMethodId(rs.getInt("delivery_method_id"));
						bdb.setUserId(rs.getInt("user_id"));
						bdb.setDeliveryMethodPrice(rs.getInt("price"));
						bdb.setDeliveryMethodName(rs.getString("name"));



					}

					System.out.println("searching BuyDataBeans by buyID has been completed");

					return bdb;
				} catch (SQLException e) {
					System.out.println(e.getMessage());


				} finally {
					if (con != null) {
						con.close();
					}
				}
				return null;
			}


	public static ArrayList<ItemDataBeans> itemdetails(int buyId)throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM m_item JOIN t_buy_detail"
					+ " ON m_item.id = t_buy_detail.item_id"
					+ " WHERE t_buy_detail.buy_id = ?");

			st.setInt(1,buyId);

			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();


			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));


				itemList.add(idb);
			}

			System.out.println("searching ItemDataBeansList by BuyID has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

		//ユーザー情報購入履歴
		public static ArrayList<BuyDataBeans> userdetail(int userId) throws SQLException{
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();

				st = con.prepareStatement(
						"SELECT * FROM t_buy"
								+ " JOIN m_delivery_method"
								+ " ON t_buy.delivery_method_id = m_delivery_method.id"
								+ " WHERE t_buy.user_id = ?");
				st.setInt(1,userId);




				ResultSet rs = st.executeQuery();

				ArrayList<BuyDataBeans> buydatabeans = new ArrayList<BuyDataBeans>();


				while (rs.next()) {
					BuyDataBeans bdb = new BuyDataBeans();
					bdb.setId(rs.getInt("id"));
					bdb.setTotalPrice(rs.getInt("total_price"));
					bdb.setBuyDate(rs.getTimestamp("create_date"));
					bdb.setDelivertMethodId(rs.getInt("delivery_method_id"));
					bdb.setUserId(rs.getInt("user_id"));
					bdb.setDeliveryMethodPrice(rs.getInt("price"));
					bdb.setDeliveryMethodName(rs.getString("name"));

					buydatabeans.add(bdb);
				}

				System.out.println("searching BuyDataBeans by buyID has been completed");

				return buydatabeans;
			} catch (SQLException e) {
				System.out.println(e.getMessage());


			} finally {
				if (con != null) {
					con.close();
				}
			}
			return null;
		}
	}


