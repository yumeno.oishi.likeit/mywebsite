package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import Beans.UserDataBeans;
import model.DBManager;

public class Userdao {


//ログインサーブレット
public UserDataBeans findByLoginInfo(String loginId, String password) {
	 Connection conn = null;

	 try {
         // データベースへ接続
         conn = DBManager.getConnection();

         // SELECT文を準備
         String sql = "SELECT * FROM t_user WHERE login_id = ? and login_password = ?";

         // SELECTを実行し、結果表を取得
         PreparedStatement pStmt = conn.prepareStatement(sql);
         pStmt.setString(1, loginId);
         pStmt.setString(2, password);
         ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
         if (!rs.next()) {
             return null;
         }

         // 必要なデータのみインスタンスのフィールドに追加
         //dbに合わせる
         String loginIdData = rs.getString("login_id");
         String Password = rs.getString("login_password");
         return new UserDataBeans(loginIdData, Password);


	  } catch (SQLException e) {
          e.printStackTrace();
          return null;
      } finally {
          // データベース切断
          if (conn != null) {
              try {
                  conn.close();
              } catch (SQLException e) {
                  e.printStackTrace();
                  return null;

}

}
      }
}

//新規登録

public void signup(String name,String address,String loginId,String password) {

	Connection conn = null;

	   try {
		// データベースへ接続
        conn = DBManager.getConnection();

        String sql ="INSERT INTO t_user (name,address,login_id,login_password,create_date) VALUES (?, ?, ?, ?,now())";

        PreparedStatement pStmt = conn.prepareStatement(sql);
        //()の中
        pStmt.setString(1, name);
        pStmt.setString(2, address);
        pStmt.setString(3, loginId);
        pStmt.setString(4,password);

        int result = pStmt.executeUpdate();


	    } catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	    	 // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	    }
}
	    }
}
	public UserDataBeans findloginid(String loginid) {
		 Connection conn = null;

		   try {
			   conn = DBManager.getConnection();

			   // SELECT文を準備
			   //userall.jspから持ってきたidを元にしてdbから検索
	           String sql = "SELECT * FROM t_user WHERE login_id  = ?";
	           PreparedStatement pStmt = conn.prepareStatement(sql);
	           pStmt.setString(1, loginid);
	           ResultSet rs = pStmt.executeQuery();

	           // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	           if (!rs.next()) {
	               return null;
	           }

	           // dbから検索してきた必要なデータを変数に代入
	           //userクラスのデータ型に合わせる
	           int id1 = rs.getInt("id");
	           String Name = rs.getString("name");
	           String Address = rs.getString("address");
	           String LoginId = rs.getString("login_id");
	           String Password = rs.getString("login_password");
	           Date Createdate = rs.getDate("create_date");

	           return new UserDataBeans(id1,Name,Address,LoginId,Password,Createdate);

		   } catch (SQLException e) {
		          e.printStackTrace();
		          return null;
		      } finally {
		          // データベース切断
		          if (conn != null) {
		              try {
		                  conn.close();
		              } catch (SQLException e) {
		                  e.printStackTrace();
		                  return null;



	}

}
}
	}
	//ユーザ情報更新
	public static void updateUser(UserDataBeans udb) throws SQLException {

		// 更新された情報をセットされたJavaBeansのリスト
				UserDataBeans updatedUdb = new UserDataBeans();
				Connection con = null;
				PreparedStatement st = null;


				try {

					con = DBManager.getConnection();
					st = con.prepareStatement("UPDATE t_user SET name=?, login_id=?, address=? WHERE id=?;");
					st.setString(1, udb.getName());
					st.setString(2, udb.getLoginId());
					st.setString(3, udb.getAddress());
					st.setInt(4, udb.getId());
					st.executeUpdate();
					System.out.println("update has been completed");

					st = con.prepareStatement("SELECT name, login_id, address FROM t_user WHERE id=" + udb.getId());
					ResultSet rs = st.executeQuery();

					while (rs.next()) {

						updatedUdb.setName(rs.getString("name"));
						updatedUdb.setLoginId(rs.getString("login_id"));
						updatedUdb.setAddress(rs.getString("address"));
					}
						st.close();
						System.out.println("searching updated-UserDataBeans has been completed");

					} catch (SQLException e) {
						System.out.println(e.getMessage());
						throw new SQLException(e);
					} finally {
						if (con != null) {
							con.close();
						}
						}


					}





}


