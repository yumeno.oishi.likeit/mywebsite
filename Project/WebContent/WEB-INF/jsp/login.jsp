<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>login</title>
    <!-- login.cssの読み込み -->
    <link href="css/login.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                   <font size="+3">
                    <a class="nav-link" href="login">漫画</a>
                    </font>
                </li>
            </ul>
        </nav>
    </header>
      <br>
<br>

    <!-- ログインフォーム -->
    <form action="login" method="post">

        <center>
             <h3>ログイン</h3>

            <div align="right">
            <a href="SignUp" class="cp_link" >新規登録</a></div>

        <div class="cp_iptxt">
        <!-- inputタグにはname必須 ビーンズに合わせる -->
            <input type="text" placeholder="ログインID" name="loginId">
	<i class="fa fa-user fa-lg fa-fw" aria-hidden="true"></i>
        </div>
        <div class="cp_iptxt">
            <input type="text" placeholder="パスワード" name="password">
	<i class="fa fa-envelope fa-lg fa-fw" aria-hidden="true"></i>
        </div>
        <div class="col-4 mx-auto">
            <button class="cp_button06" type="submit" >ログイン</button>
 		</div>
 		</fieldset>
        </center>

    </form>


</body></html>
