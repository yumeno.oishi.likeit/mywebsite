<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>home</title>
    <!-- home.cssの読み込み -->
    <link href="css/home.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                <font size="+3">
                   <a class="nav-link" href="Home">漫画</a>
                   </font>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="Mypage">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cart">カート</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login">ログアウト</a>
                </li>
            </ul>
        </nav>

    </header>
<br>

    <div class="slide" id="makeImg">
        <img src="images/natumebana.jpg"   alt="夏目">
        <img src="images/kimetubana.jpeg"    alt="鬼滅">
        <img src="images/manzi.jpeg"    alt="東リベ">

      </div>
<br>
      <form action="ItemSearch" method="Get">
        <div class="searchArea" id="makeImg">
            <input type="text" id="inText" class="searchText" name="search_word">
            <div class="searchButton">
                   <button type="submit" id="search"><img class="searchImg" src="images/%E8%99%AB%E7%9C%BC%E9%8F%A1%E3%81%AE%E3%82%A2%E3%82%A4%E3%82%B3%E3%83%B3.png"></button>
            </div>
            </div>
            </form>

    <!-- おすすめ商品欄 -->
    <div class="container mt-5">
        <div class="items">
            <h3>おすすめ商品</h3>


        </div>
         <br>
        <div class="section">
            <div class="row">
                  <c:forEach var="item" items="${itemList}">
                <!--   商品1   -->
                <div class="img">

                        <div class="card-image">
                            <a href="Item?item_id=${item.id}"><img src="images/${item.fileName}" class="Recommended_img" ></a>
                        </div>

                        <div class="card-content">
                        <font size="3">
                            <span class="card-title">${item.name}</span>
                           </font>
                        </div>

                    </div>
                     </c:forEach>
                </div>




            </div>
        </div>
    </div>

</body>

</html>
