<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>cart</title>
    <!-- home.cssの読み込み -->
    <link href="css/cart.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <font size="+3">
                    <a class="nav-link" href="Home">漫画</a>
                    </font>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="Mypage">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cart">カート</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="container mb-5">
    ${cartActionMessage}
        <div class="col-2 mx-auto mt-5">

            <h3 class="ml-4">カート</h3>
        </div>


        <form action="ItemDelete" method="POST">
         <center>
        <div class="row">
            <div class="col s12">
                <div class="col s6 center-align">
                    <input class="button" type="submit" name="action" value="削除">
                </div>
                <div class="col s6 center-align">
                    <input class="button" type="button" name="action" onClick="location.href='Buy'" value="購入">
                </div>

            </div>
        </div>
        </center>
        <br>
         <div class="row">

        <c:forEach var="item" items="${cart}" varStatus="status">
         <div class="card" style="width: 17rem;">
            <img class="card-img-top" src="images/${item.fileName}">
            <div class="card-body">
                <h5 class="card-title">${item.name}</h5>
                <p class="card-text">${item.formatPrice}円</p>

                <input type="checkbox" id="${status.index}" name="delete_item_id_list" value="${item.id}" /><label for="${status.index}">削除</label>
 </div>
  </div>
            </c:forEach>



   </div>

            </form>
              </div>



</body></html>
