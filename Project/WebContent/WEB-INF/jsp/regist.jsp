<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>home</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                   <font size="+3">
                    <a class="nav-link" href="login">漫画</a>
                    </font>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">

            </ul>
        </nav>
    </header>


    <div class="container mt-5">
        <div class="col-2 mx-auto">
            <h3>ユーザ登録</h3>
        </div>
        <!--   登録フォーム   -->
<form class="form-group" action="SignUp" method="post">
        <div class="col-5 mx-auto">
            <table>
                <tbody>
                    <tr height="80">
                        <td class="center" width="200">名前</td>
                        <td class="center"><input type="text" name="name"></td>
                    </tr>
                    <tr height="80">
                        <td class="center">住所</td>
                        <td class="center"><input type="text" name="address"></td>
                    </tr>
                    <tr height="80">
                        <td class="center">ログインID</td>
                        <td class="center"><input type="text" name="loginId"></td>
                    </tr>
                    <tr height="80">
                        <td class="center">パスワード</td>
                        <td class="center"><input type="password" name="password"></td>
                    </tr>
                    <tr height="80">
                        <td class="center">パスワード(確認用)</td>
                        <td class="center"><input type="password" name="password1"></td>
                    </tr>


                </tbody>
            </table>


            <div class="col-5 mx-auto mt-4">
                <input type="submit" class="button" value="登録">
            </div>
        </div>
        </form>
           </div>





</body></html>
