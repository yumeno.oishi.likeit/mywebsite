<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>home</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                   <font size="+3">
                    <a class="nav-link" href="Home">漫画</a>

                    </font>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
            <li class="nav-item">
                    <a class="nav-link" href="Mypage">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cart">カート</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login">ログアウト</a>
                </li>

            </ul>
        </nav>
    </header>


    <div class="container mt-5">
        <div class="col-3 mx-auto">
            <h4>購入が完了しました</h4>
        </div>
        <br>
        <div>
            <div class="col-6 mx-auto row">
                <div class="col-6 center-align">
                    <a href="Home" class="cp_link">引き続き買い物をする</a>
                </div>
                <div class="col-6 center-align">
                    <a href="Mypage" class="cp_link">ユーザー情報へ</a>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="col-2 mx-auto">
            <h4>購入詳細</h4>
        </div>

        <div class="row col-7 mx-auto">

            <div>
                <div>
                    <table class="meiji-table" border="1" bordercolor="#E5E5E5">
                        <thead>
                            <tr>
                                <th width="400" height="50">購入日時</th>
                                <th width="200">配送方法</th>
                                <th width="200">合計金額</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="600" height="80">${resultBDB.formatDate}</td>
                                <td>${resultBDB.deliveryMethodName}</td>
                                <td>${resultBDB.formatTotalPrice}円</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- 詳細 -->
        <div class="col-6 mx-auto">
           <table class="meiji-table" border="1" bordercolor="#E5E5E5">
                <thead>
                    <tr>
                        <th width="900" height="50">商品名</th>
                        <th width="500">単価</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var="buyIDB" items="${buyIDBList}" >

                    <tr>
                        <td width="300" height="80">${buyIDB.name}</td>
                        <td width="300" height="80">${buyIDB.formatPrice}円</td>
                    </tr>
                    </c:forEach>

                    <tr>
                        <td width="300" height="80">${resultBDB.deliveryMethodName}</td>
                        <td width="300" height="80">${resultBDB.deliveryMethodPrice}円</td>
                    </tr>
                    <br>
                    <br>
                    <br>
                </tbody>
            </table>
        </div>

    </div>

</body></html>
