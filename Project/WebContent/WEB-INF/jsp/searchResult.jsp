<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>myPage</title>
    <!-- searchResult.cssの読み込み -->
    <link href="css/searchResult.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <font size="+3">
                    <a class="nav-link" href="Home">漫画</a>
                    </font>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="Mypage">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cart">カート</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <div class="container mt-5">
        <!-- 検索結果 -->
        <div style="border-left: 10px solid #E8BFBE; border-bottom: 2px solid #E8BFBE; padding-left: 8px; font-weight: bold; font-size: 220%; color: #A06D6B;">検索結果</div>
        <div class="col-2 mx-auto mb-3">
        <br>
            <h6 class="ml-2">${itemCount}件</h6>
        </div>
        <br>
        <div class=" row">
        <c:forEach var="item" items="${itemList}" varStatus="status">

            <!--   商品1   -->
            <div class="col s12 m3">

                    <div class="card-image">
                       <a href="Item?item_id=${item.id}&page_num=${pageNum}"><img src="images/${item.fileName}"></a>
                    </div>
                    <div class="card-content">
                        <span class="card-title">${item.name}</span>
                        <p>${item.formatPrice}円
                        </p>

                </div>
            </div>
            <c:if test="${(status.index + 1) % 4 == 0}">

				</c:if>
				</c:forEach>
				</div>
				</div>






</body>

</html>
