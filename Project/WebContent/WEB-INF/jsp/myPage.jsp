<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>myPage</title>
    <!-- myPage.cssの読み込み -->
    <link href="css/myPage.css" rel="stylesheet" type="text/css" />
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <font size="+3">
                    <a class="nav-link" href="Home">漫画</a>
                    </font>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="Mypage">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cart">カート</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

   <center>
           <div class="buy col-8 mx-auto mt-5">
    <div class="mybox">

            <form action="UpdateResult" method="POST">

                <table>
                    <tbody>
                        <tr>
                            <td width="100" height="70">名前</td>
                            <td width="200"><input type="text" name="user_name_update" value="${mypage.name}"></td>
                        </tr>
                        <tr>
                            <td height="70">ログインID</td>
                            <td><input type="text" name="login_id_update" value="${mypage.loginId}"></td>
                        </tr>
                        <tr>
                            <td height="100">住所</td>
                            <td><input type="text" name="user_address_update" value="${mypage.address}"></td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <input type="submit" class="button" value="更新">
                <br>
                <br>
                </form>

            </div>
        </div>
        </center>

        <!-- 購入履歴 -->
        <div class="buy col-7 mx-auto mt-4">
            <div class="mybox1">
            <table class="table_width">
                <thead>
                    <tr>
                        <th></th>
                        <th class="center">購入日時</th>
                        <th class="center">配送方法</th>
                        <th class="center">購入金額</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var="buy" items="${buy}" >

                    <tr>
                        <td class="center"><a href="UserBuyHistory?buyId=${buy.id}" class="btn_detail">詳細</a></td>
                        <td class="center">${buy.formatDate}</td>
                        <td class="center">${buy.deliveryMethodName}</td>
                        <td class="center">
                           ${buy.formatTotalPrice}円</td>
                    </tr>
                     </c:forEach>


                </tbody>
            </table>
        </div>
        </div>

</body></html>
