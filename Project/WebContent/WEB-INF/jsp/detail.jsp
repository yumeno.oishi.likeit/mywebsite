<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>detail</title>

    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />

     <!-- detail.cssの読み込み -->
    <link href="css/detail.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />

</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                   <font size="+3">
                    <a class="nav-link" href="Home">漫画</a>
                    </font>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="nav-link" href="Mypage">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cart">カート</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>

    <!-- おすすめ商品欄 -->
    <div class="container mt-5">

        <div>
        <form action="ItemAdd" method="POST">
		<input type="hidden" name="item_id" value="${item.id}">
		<div class="text-right">
           <button class="button"  type="submit">カートに追加</button>
         </div>
        </form>
        </div>
        <br>
        <br>


        <div class="row">
            <div class="col-4"><img src="images/${item.fileName}"></div>
            <div class="col-7">
                <div class="mt-5">
                    <h3>${item.name}</h3>
                </div>
                <div>
                    <h5>${item.formatPrice}円</h5>
                </div>
                <br>
                <div style="padding:10px;border:3px dotted #ffb6c1;">
                    <h5 class="product-detail">${item.detail}</h5>
                </div>
            </div>
        </div>
    </div>

</body>

</html>
