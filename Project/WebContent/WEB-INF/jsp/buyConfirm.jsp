
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>home</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                <font size="+3">
                    <a class="nav-link" href="Home">漫画</a>
                    </font>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
             <li class="nav-item">
                    <a class="nav-link" href="Mypage">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cart">カート</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login">ログアウト</a>
                </li>

            </ul>
        </nav>
    </header>


    <div class="container mt-5">
        <div class="col-1 mx-auto">
            <h3>購入</h3>
        </div>
        <br>
        <div class="col-6 mx-auto">
            <div class="row">
                <table class="meiji-table" border="1" bordercolor="#E5E5E5">
                    <thead>
                        <tr>
                             <th width="500" height="50">商品名</th>
                            <th width="200">単価</th>
                            <th width="200">小計</th>
                        </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="cartInItem" items="${cart}" >

                        <tr>
                            <td width="800" height="80">${cartInItem.name}</td>
                            <td>${cartInItem.formatPrice}円</td>
                            <td>${cartInItem.formatPrice}円</td>
                        </tr>
                        </c:forEach>

                        <tr>
                            <td width="900" height="80">${bdb.deliveryMethodName}</td>
                            <td></td>
                            <td>${bdb.deliveryMethodPrice}円</td>
                        </tr>

                        <tr>
                            <td height="50"></td>
                            <td>合計</td>
                            <td>${bdb.formatTotalPrice}円</td>
                        </tr>


                    </tbody>
                </table>
            </div>
            <br>
            <div class="row">
                <div class="col-4 mx-auto">
                <form action="BuyResult" method="post">
                    <input class="button" type="submit" value="購入">
               </form>
                </div>
            </div>
        </div>
    </div>

</body></html>
