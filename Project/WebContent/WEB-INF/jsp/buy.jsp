<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>home</title>
    <!-- header.cssの読み込み -->
    <link href="css/header.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrapの読み込み -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- common.cssの読み込み -->
    <link href="css/common.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <!-- ヘッダー -->
    <header>
        <nav class="navbar navbar-dark navbar-expand  flex-md-row header-one">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                <font size="+3">
                    <a class="nav-link" href="Home">漫画</a>
                    </font>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
             <li class="nav-item">
                    <a class="nav-link" href="Mypage">マイページ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cart">カート</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login">ログアウト</a>
                </li>

            </ul>
        </nav>
    </header>


    <div class="container mt-5">
        <div class="col-3 mx-auto">
            <h3>カートアイテム</h3>
        </div>
        <br>

        <div class="col-5 mx-auto">

         <form action="BuyConfirm" method="POST">
            <div class="row">
                <table class="meiji-table" border="1" bordercolor="#E5E5E5">
                    <thead>
                        <tr>
                           <th class="center" width="1000" height="60"> 商品名</th>
                            <th class="center" width="800" height="60"> 単価</th>
                            <th class="center" width="200" height="60" > 小計</th>
                        </tr>
                    </thead>
                    <tbody>

                    <c:forEach var="cartInItem" items="${cart}" >

                        <tr>
                            <td class="center"  height="80">${cartInItem.name}</td>
                            <td class="center" height="60">${cartInItem.formatPrice}円</td>
                            <td class="center">${cartInItem.formatPrice}円</td>
                        </tr>
                        </c:forEach>

                        <tr>
                            <td class="center" width="300" height="80"></td>
                            <td class="center"></td>
                           <td class="center" width="300" height="50">
                                <div class="input-field col s8 offset-s2 ">
                                    <label>配送方法</label>
                                    <select name="delivery_method_id">
                                    <c:forEach var="dmdb" items="${dmdbList}" >
                                        <option value="${dmdb.id}">${dmdb.name}</option>
                                        </c:forEach>

                                    </select>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="row">
                <div class="col-7 mx-auto">
                    <button class="button"  type="submit" name="action">購入確認</button>
                </div>
            </div>
            </form>
        </div>
    </div>

</body></html>
